# Készítsünk egy Rails 5 alkalmazást. 

- Legyen benne bejelentkezés ahol a felhasználók be tudnak lépni.
- Legyen benne felhasználó lista (email, username, first_name, last_name)
- A felhasználók adatait lehessen szerkeszteni
- Az alkalmazásban legyenek jogok-ok (name)
- Egy felhasználónak több jogot-t is meg lehessen adni
- Lehessen csoportokat létrehozni, amik jogokat tartalmaznak. (name)
- Egy felhasználó több csoportba is tartozhat.

### Lehetőleg az alábbi gem-eket használjuk:

- Devise
- Simple_form
      